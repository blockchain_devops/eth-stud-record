pragma solidity ^0.5.0;

contract StudentList {
  uint public studentCount = 0;

  struct Student {
    uint id;
    string name;
    bool graduated;
  }

  mapping(uint => Student) public students;

  event StudentCreated(
    uint id,
    string name,
    bool graduated
  );

  event StudentGraduated(
    uint id,
    bool graduated
  );

  constructor() public {
    createStudent("John Doe");
  }

  function createStudent(string memory _name) public {
    studentCount ++;
    students[studentCount] = Student(studentCount, _name, false);
    emit StudentCreated(studentCount, _name, false);
  }

  function toggleGraduated(uint _id) public {
    Student memory _Student = students[_id];
    _Student.graduated = !_Student.graduated;
    students[_id] = _Student;
    emit StudentGraduated(_id, _Student.graduated);
  }

}
