//Import the StudentList smart contract
const StudentList = artifacts.require('./StudentList.sol')

//Use the contract function to write all tests
//variable: account => all accounts in blockchain
contract('StudentList', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the studentlist object for testing
    before(async () => {
        this.studentList = await StudentList.deployed()
    })

    //Testing the deployed student contract
    it('deploys successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.studentList.address
        //Test for valid address
        assert.notEqual(address, 0x0)
        assert.notEqual(address, '')
        assert.notEqual(address, null)
        assert.notEqual(address, undefined)
    })
    //Testing the content in the contract
    it('lists students', async () => {
        //Get the content in the contract to be tested
        const studentCount = await this.studentList.studentCount()
        const student = await this.studentList.students(studentCount)
        //Test the student id
        assert.equal(student.id.toNumber(), studentCount.toNumber())
        //Test the student name
        assert.equal(student.name, 'John Doe')
        //Test the graduated boolean value
        assert.equal(student.graduated, false)
        //Test the number of student in list
        assert.equal(studentCount.toNumber(), 1)
    })
    //Testing the graduated boolean variable in the contract
    it('toggles graduated completion', async () => {
        const result = await this.studentList.toggleGraduated(1)
        const student = await this.studentList.students(1)
        assert.equal(student.graduated, true)
        const event = result.logs[0].args
        assert.equal(event.id.toNumber(), 1)
        assert.equal(event.graduated, true)
      })
})