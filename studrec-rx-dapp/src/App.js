//Step 2: change the base code to use Component
import React, { Component } from 'react'
import Web3 from 'web3'
import './App.css';
//Step 3 : Import the ABI
import { STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS } from './config'
import StudentList from './StudentList'
//We replace function with a component for better management of code
class App extends Component {
  //componentDidMount called once on the client after the initial render
  //when the client receives data from the server and before the data is displayed. 
  componentDidMount() {
    this.loadBlockchainData()
  }
  //create a web3 connection to using a provider, MetaMask to connect to 
  //the ganache server and retrieve the first account
  async loadBlockchainData() {
    // const web3 = new Web3(Web3.givenProvider || "http://ganache-cli-server:7545")
    const web3 = new Web3(Web3.givenProvider ||
      'https://ropsten.infura.io/v3/03b17b19872c4831b0cdc07e0ae0fbf7'
    )
    const accounts = await web3.eth.getAccounts()
    this.setState({ account: accounts[0] })
    //Step 4: load the student list from the blockchain
    const studentList = new web3.eth.Contract(STUDENT_LIST_ABI, STUDENT_LIST_ADDRESS)
    //Keep the studentList in the current state
    this.setState({ studentList })
    //Get the number of student records in the blockchain
    const studentCount = await studentList.methods.studentCount().call()
    //Store this value in the current state as well
    this.setState({ studentCount })
    //Use an iteration to extract each students info and store
    //them in an array
    this.state.students=[]
    for (var i = 1; i <= studentCount; i++) {
      const student = await studentList.methods.students(i).call()
      this.setState({
        students: [...this.state.students, student]
      })
    }
  }
  //Initialise the variables stored in the state
  constructor(props) {
    super(props)
    this.state = {
      account: '',
      studentCount: 0,
      students: [],
      loading: true,
    }
    this.createStudent = this.createStudent.bind(this)
    this.toggleGraduated = this.toggleGraduated.bind(this)
  }
  createStudent(name) {
    this.setState({ loading: true })
    this.state.studentList.methods.createStudent(name).send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
      this.loadBlockchainData()
    })
  }
  toggleGraduated(studentId) {
    this.setState({ loading: true })
    this.state.studentList.methods.toggleGraduated(studentId).send({ from: this.state.account })
    .once('receipt', (receipt) => {
      this.setState({ loading: false })
    })
  }
  //Display the account and student list
  render() {
    return (
      <div className="container">
<h1>Hello, World!</h1>
<p>Your account: {this.state.account}</p>
<StudentList
createStudent={this.createStudent}
toggleGraduated={this.toggleGraduated}/>

<p/>
  <ul id="studentList" className="list-unstyled">
    {
      //This gets the each student from the studentList
      //and pass them into a function that display the 
      //details of the student
      this.state.students.map((student, key) => {
        return (
          <li className="list-group-item checkbox" key={key}>
            <span className="name alert">{student.id}. {student.name}</span>
            <input 
            className="form-check-input" 
            type="checkbox"
            name={student.id}
            defaultChecked={student.graduated}
            ref={(input) => {
              this.checkbox = input
            }}
            onClick={(event) => {
              this.toggleGraduated(this.checkbox.name) }} />
            <label class="form-check-label" >Graduated</label>
          </li>
        )
      }
      )
    }
  </ul>
</div>
    );
  }
}
export default App;